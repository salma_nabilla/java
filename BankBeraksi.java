class Bank {
	int saldo;

	public Bank(int saldo){
		this.saldo=saldo;
		System.out.println("Selamat Datang di Bank ABC");
		System.out.println("Saldo saat ini: "+saldo);
		System.out.println("");
	}

	public void simUang(int simUang){
		saldo = saldo + simUang;
	}

	public void ambilUang(int ambilUang){
		saldo = saldo - ambilUang;
	}

	public int getSaldo(){
		return (saldo);
	}
}

class BankBeraksi {
	public static void main(String[] args) {
		Bank tes = new Bank(100000);
		tes.simUang(500000);
		System.out.println("Simpan uang: Rp 500000");
		System.out.println("Saldo saat ini: Rp "+tes.getSaldo());
		System.out.println("");
		tes.ambilUang(150000);
		System.out.println("Ambil uang: Rp 150000 ");
		System.out.println("Saldo saat ini: "+tes.getSaldo());
	}
}